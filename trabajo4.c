#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <mpi.h>

int main(){
    int n,n2,n3;
    int pasa = 0, i1,j1,i2,j2;
    int cont1=0, cont2=0;
    int rank,size;
    srand(time(NULL));
    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    
    //T1
    if(rank==0){
        
        while (pasa==0){
            printf("Ingrese un numero entre 300 y 400: \n");
            scanf("%d", &n);
            if (n>=300 && n<=400){
                pasa = 1;
                    //Se envia el valor de n a todos los procesadores
                for (int i = 0; i < size; i++){
                    MPI_Send(&n, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
                }
            }else{
                printf("Porfavor ingrese un numero solamente entre 300 y 400\n");
           }
        }
         
    }

    //Se recibe el valor de n a todos los procesadores T2,T3,T4,T5
    MPI_Recv(&n, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    if(n % 2!=0){
        n2 = (int) (n/2) +1;
        n = n2*2;
    }else{
        n2=n/2;
    }
    n3=n*n;
    int matriz[n][n];
    int camino1[n], camino2[n];
    int aux1[n][n];
    if(rank==0){//T2
        for (int i = 0; i < n2; i++){
            for (int j = 0; j < n; j++){
                if (i==j){
                    matriz[i][j]=0;
                }else{
                    int a = rand()%100;
                    if (a >= 90 && a <= 100){
                        matriz[i][j]=100000;
                    }else{
                        matriz[i][j]=a;
                    }
                }
            }
            
        }
        MPI_Send(&matriz, n3, MPI_INT, 1, 0, MPI_COMM_WORLD);
    }
    if(rank==2){//T4
        for (int i = n2; i < n; i++){
            for (int j = 0; j < n; j++){
                if (i==j){
                    aux1[i][j]=0;
                }else{
                    int a = rand()%100;
                    if (a >= 90 && a <= 100){
                        aux1[i][j]=100000;
                    }else{
                        aux1[i][j]=a;
                    }
                }
            }
        }
        MPI_Send(&aux1, n3,MPI_INT, 1, 0, MPI_COMM_WORLD);
    }

    if(rank==1){//T6
        MPI_Recv(&matriz, n3, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Recv(&aux1, n3, MPI_INT, 2, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        for (int x=n2; x < n; x++){
            for (int y=0; y < n; y++){
                matriz[x][y] = aux1[x][y];
            }
        }

        MPI_Send(&matriz, n3, MPI_INT, 3, 0, MPI_COMM_WORLD);
    }

    if(rank==3){
        MPI_Recv(&matriz, n3, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        for (int k = 0; k < n; k++){
            for (int i = 0; i < n; i++){
                for (int j = 0; j < n; j++){
                    int dt = matriz[i][k];
                    if (matriz[i][j] == 100000){
                        matriz[i][j] = dt;
                    }
                }
            }
        }
        MPI_Send(&matriz, n3, MPI_INT, 2, 0, MPI_COMM_WORLD);
        MPI_Send(&matriz, n3, MPI_INT, 1, 0, MPI_COMM_WORLD);
        
    }
    if(rank==2){//primera mitad m1
        MPI_Recv(&matriz, n3, MPI_INT, 3, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        for (int x=0; x < n2; x++){
            for (int y=0; y < n; y++){
                printf("%d\t",matriz[x][y]);
            }
            printf("\n");
        }
    }
    /////////////
    if(rank==1){//segunda mitad m1
        MPI_Recv(&matriz, n3, MPI_INT, 3, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        sleep(2);
        for (int x=n/2; x < n; x++){
            for (int y=0; y < n; y++){
                printf("%d\t",matriz[x][y]);
            }
            printf("\n");
        }
        MPI_Send(&matriz, n3, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }


    if(rank==0){
        sleep(5);
        MPI_Recv(&matriz, n3, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        pasa=0;
        while (pasa==0)
        {
            printf("Ingrese las coordenadas de origen: \n");
            scanf("%d %d", &i1,&j1);
            printf("Ingrese las coordenadas de destino: \n");
            scanf("%d %d", &i2,&j2);
            printf("Origen [%d,%d]\n",i1,j1);
            printf("Destino [%d,%d]\n",i2,j2);

            if (i1>=0 && i1<=(n-1) && j1>=0 && j1<=(n-1) && j2<n && i2<n ){
                
                if (i1==i2){// Si el valor de origen y destino esta en la misma fila
                    int val = 0;
                    int i = i1, j= j1;
                    if(j1<j2){//si esta a la derecha
                        while (val==0){
                            camino1[cont1] = matriz[i][j];
                            if(j==j2)
                                val=1;
                            j++;
                            cont1++;
                        }
                    }else if(j1>j2){//si esta a la izquierda
                        while (val==0){
                            camino1[cont1] = matriz[i][j];
                            if(j==j2)
                                val=1;
                            j--;
                            cont1++;
                        }
                    }
                    
                }else if (j1==j2){//Si el valor de origen y destino esta en la misma columna
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){//baja verticalmente
                        camino1[cont1] = matriz[i][j];
                        if(i==i2)
                            val=1;
                        i++;
                        cont1++;
                    } 
                }else if (i1==j1 && i2==j2){//Si estan en la diagonal principal
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        camino1[cont1] = matriz[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j++;
                        cont1++;
                    }
                }else if(i1<j1 && i2<j2 && (j1-i1)==(j2-i2)){//Si estan sobre la diagonal principal
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        camino1[cont1] = matriz[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j++;
                        cont1++;
                    }
                }else if(i1>j1 && i2>j2 && (i1-j1)==(i2-j2)){//Si estan bajo la diagonal principal
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        camino1[cont1] = matriz[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j++;
                        cont1++;
                    }
                }else if((i1+j1) == (n-1) && (i2+j2)== (n-1)){//Si estan en la diagonal segundaria
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        camino1[cont1] = matriz[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j--;
                        cont1++;
                    }
                }else if((i1+j1)==(i2+j2)){//Si estan sobre y bajo la diagonal segundaria
                    int val = 0;
                    int i = i1, j= j1;
                    while (val==0){
                        camino1[cont1] = matriz[i][j];
                        if(i==i2 && j==j2)
                            val=1;
                        i++;
                        j--;
                        cont1++;
                    }
                }else{//otras
                    int i = i1, j= j1;
                    if (j<j2){//derecha
                        int val =0;
                        while (val==0){
                            camino2[cont2] = matriz[i][j];
                            if(i==i2){
                                if(j<j2){ // va a la derecha
                                    int val2 = 0;
                                    while (val2==0){
                                        camino2[cont2] = matriz[i][j];
                                        if(i==i2 && j==j2){
                                            val2=1;
                                            val = 1;
                                        }
                                        cont2++;
                                        j++;
                                    }
                                }else if(j>j2){//a la izquierda
                                    int val2 = 0;
                                    while (val2==0){
                                        camino2[cont2] = matriz[i][j];
                                        if(i==i2 && j==j2){
                                            val2= 1;
                                            val = 1;
                                        }
                                        cont2++;
                                        j--;
                                    }
                                }
                            }else if(j==j2){//abajo
                                int val2 = 0;
                                while (val2==0){
                                    camino2[cont2] = matriz[i][j];
                                    if(i==i2 && j==j2){
                                        val2= 1;
                                        val = 1;
                                    }
                                    cont2++;
                                    i++;
                                }
                            }else{
                                cont2++;
                                i++;
                                j++;
                            }
                            
                        }
                    }else if(j>j2){//izquierda
                        int val =0;
                        while (val==0){
                            camino2[cont2] = matriz[i][j];
                            if(i==i2){
                                if(j<j2){ // va a la derecha
                                    int val2 = 0;
                                    while (val2==0){
                                        camino2[cont2] = matriz[i][j];
                                        if(i==i2 && j==j2){
                                            val2=1;
                                            val = 1;
                                        }
                                        cont2++;
                                        j++;
                                    }
                                }else if(j>j2){//a la izquierda
                                    int val2 = 0;
                                    while (val2==0){
                                        camino2[cont2] = matriz[i][j];
                                        if(i==i2 && j==j2){
                                            val2= 1;
                                            val = 1;
                                        }
                                        cont2++;
                                        j--;
                                    }
                                }
                            }else if(j==j2){//abajo
                                int val2 = 0;
                                while (val2==0){
                                    camino2[cont2] = matriz[i][j];
                                    if(i==i2 && j==j2){
                                        val2= 1;
                                        val = 1;
                                    }
                                    cont2++;
                                    i++;
                                }
                            }else{
                                cont2++;
                                i++;
                                j--;
                            }
                            
                        }
                    }
                }
                //RESULTADOS
                printf("Orige: %d  Destino: %d  Camino: ",matriz[i1][j1], matriz[i2][j2]);
                if(cont1>cont2){                //printf(" %d ", matriz[i][j]);
                    for (int i = 0; i < cont1; i++){
                        printf(" %d -> ", camino1[i]);
                    }
                    printf("\n");
                }else{
                    for (int i = 0; i < cont2; i++){
                        printf(" %d -> ", camino2[i]);
                    }
                    printf("\n");
                }    

                pasa = 1;
            }else{
                printf("Porfavor ingrese uel rango de coordenadas validas\n");
            }

        }
    }

    MPI_Finalize();
    return 0;
}